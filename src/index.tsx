import React from 'react';
import App from './App';
import { app } from './models/App';
import { run } from './runIndex';

run(app, <App />);