import { Lang } from './Lang';

export type Locale = 'fi' | 'sv' | 'en';
export const lang = new Lang();