import React from 'react';
import bbox from '@turf/bbox'
import combine from '@turf/combine'
import { featureCollection } from '@turf/helpers'
import { Map } from 'mapbox-gl';
import Stand from '../models/Stand';

const useBounds = (
  map?: Map,
  stands?: Stand[]
) => {
  React.useEffect(
    () => {
      if (!map || !stands) return;
      const bb = bbox(combine(featureCollection([...stands.map(s => s.geoJSON)])));
      map.fitBounds(
     bb as [number, number, number, number],
        { padding: 20, linear: true },
      );
    },
    [map, stands],
  );
}

export default useBounds;
